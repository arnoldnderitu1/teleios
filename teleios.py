from app import create_app, db, cli
from flask import url_for
from datetime import datetime
from app.auth.models.employee import Employee
from app.auth.models.employee import Role
from app.main.models.module import Module, ModuleCategory
from app.main.models.user import UserGroup, User
from app.main.models.language import Lang
from app.main.models.partner import Partner
from app.main.models.rule import Rule
from flask_migrate import Migrate, upgrade

app = create_app()
cli.register(app)


@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'Employee': Employee}


@app.cli.command()
def fetchmail():
    """Scheduled job for fetching incoming emails."""
    print(str(datetime.utcnow()), 'Fetching emails')
    # user = User(fname='Alfred')
    # db.session.add(user)
    # db.session.commit()
    # return url_for('fetchmail.fetchmail')
    return "success"


@app.cli.command()
def deploy():
    """Run deployment tasks."""
    # migrate database to latest revision
    upgrade()

    # create or update user roles
    Role.insert_roles()
    Employee.dummy_employees()
    # Module.insert_modules()
    ModuleCategory.insert_categories()
    UserGroup.seed_user_groups()
    Lang.seed_language()
    Partner.insert_partners()
    User.insert_users()
    Rule.seed_rules()
    # SalesDocument.insert_documents()
    # SalessubDocument.insert_subdocuments()
