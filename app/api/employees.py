from flask import jsonify, request, url_for, abort
from app import db
from app.auth.models.employee import Employee
from app.api import bp
from app.api.auth import token_auth
from app.api.errors import bad_request


@bp.route('/employees/<int:id>', methods=['GET'])
@token_auth.login_required
def get_employee(id):
    return jsonify(Employee.query.get_or_404(id).to_dict())


@bp.route('/employees', methods=['GET'])
@token_auth.login_required
def get_employees():
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = Employee.to_collection_dict(
        Employee.query, page, per_page, 'api.get_employees')
    return jsonify(data)


@bp.route('/employees/<int:id>/followers', methods=['GET'])
@token_auth.login_required
def get_followers(id):
    employee = Employee.query.get_or_404(id)
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = Employee.to_collection_dict(employee.followers, page, per_page,
                                       'api.get_followers', id=id)
    return jsonify(data)


@bp.route('/employees/<int:id>/followed', methods=['GET'])
@token_auth.login_required
def get_followed(id):
    employee = Employee.query.get_or_404(id)
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = Employee.to_collection_dict(employee.followed, page, per_page,
                                       'api.get_followed', id=id)
    return jsonify(data)


@bp.route('/employees', methods=['POST'])
def create_employee():
    data = request.get_json() or {}
    if 'username' not in data or 'email' not in data or 'password' not in data:
        return bad_request('must include username, email and password fields')
    if Employee.query.filter_by(username=data['username']).first():
        return bad_request('please use a different username')
    if Employee.query.filter_by(email=data['email']).first():
        return bad_request('please use a different email address')
    employee = Employee()
    employee.from_dict(data, new_user=True)
    db.session.add(employee)
    db.session.commit()
    response = jsonify(employee.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_employee', id=employee.id)
    return response


@bp.route('/employees/<int:id>', methods=['PUT'])
@token_auth.login_required
def update_employee(id):
    if token_auth.current_user().id != id:
        abort(403)
    employee = Employee.query.get_or_404(id)
    data = request.get_json() or {}
    if 'username' in data and data['username'] != employee.username and \
            Employee.query.filter_by(username=data['username']).first():
        return bad_request('please use a different username')
    if 'email' in data and data['email'] != employee.email and \
            Employee.query.filter_by(email=data['email']).first():
        return bad_request('please use a different email address')
    employee.from_dict(data, new_user=False)
    db.session.commit()
    return jsonify(employee.to_dict())
