from app import db, current_app
from app.main.models.partner import Partner
from app.main.models.company import company_users
from config import basedir
from werkzeug.security import generate_password_hash, check_password_hash

import os
import uuid
import csv
import enum


class AddressTypeEnum(enum.Enum):
    contact = 'Contact'
    invoice = 'Invoice Address'
    delivery = 'Delivery Address'
    other = 'Other Address'
    private = "Private Address"

# ----------------------------------------------------------
# Basic user groups and users
# ----------------------------------------------------------

user_groups_helper = db.Table('user_groups_helper',
                              db.Column('user_id', db.Integer, db.ForeignKey(
                                  'user.partner_id'), primary_key=True),
                              db.Column('group_id', db.Integer, db.ForeignKey(
                                  'user_group.id'), primary_key=True)
                              )


rule_groups_helper = db.Table('rule_groups_helper',
                              db.Column('group_id', db.Integer, db.ForeignKey(
                                  'user_group.id'), primary_key=True),
                              db.Column('rule_group_id', db.Integer,
                                        db.ForeignKey('rule.id'), primary_key=True)
                              )


class UserGroup(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), nullable=False)
    users = db.relationship('User', secondary=user_groups_helper,
                            lazy='subquery', backref=db.backref('user_groups', lazy=True))
    comment = db.Column(db.Text)
    category_id = db.Column(db.Integer, db.ForeignKey(
        'module_category.id'), nullable=False)
    model_access = db.relationship('ModelAccess', backref='access_controls')
    rule_groups = db.relationship('Rule', secondary=rule_groups_helper,
                                  lazy='subquery', backref=db.backref('rules_user_groups', lazy=True))
    

    @staticmethod
    def seed_user_groups():
        group = UserGroup.query.first()
        if group is None:
            csv_file = os.path.join(
                basedir, 'app/main/data/user_groups.csv')
            with open(csv_file, 'r') as fin:
                dr = csv.DictReader(fin)
                if 'sqlite' in current_app.config['SQLALCHEMY_DATABASE_URI']:
                    for i in dr:
                        group = UserGroup(
                            id=i['id'], name=i['name'], comment=i['comment'], category_id=i['category_id'], )
                        db.session.add(group)
                        db.session.commit()


class User(Partner, db.Model):
    __tablename__ = "user"

    """ User class. A User record models Teleios user and is different
        from an employee.

        user class now inherits from partner. The partner model is
        used to store the data related to the partner: lang, name, address,
        avatar, ... The user model is now dedicated to technical data.
    """
    __table_args__ = {'extend_existing': True}

    id = db.Column(db.Integer, primary_key=True)
    partner_id = db.Column(db.Integer, db.ForeignKey("partner.id"))  
    login = db.Column(db.String(60)) 
    password = db.Column(db.String(128)) 
    partners = db.relationship(
        "Partner", backref="partner_accounts_assosciated_with_this_user", foreign_keys=[partner_id])
    groups_id = db.relationship('UserGroup', secondary=user_groups_helper,
                                lazy='subquery', backref=db.backref('_users', lazy=True))
    company_ids = db.relationship('Company', secondary=company_users,
                                  lazy='subquery', backref=db.backref('companies', lazy=True))
    notification_type = db.Column(db.String(10))

    __mapper_args__ = {'polymorphic_identity': 'user',
                       'inherit_condition': partner_id == Partner.id}

    @staticmethod
    def insert_users():
        user = User.query.first()
        if user is None:
            csv_file = os.path.join(
                basedir, 'app/main/data/users.csv')
            with open(csv_file, 'r') as fin:
                dr = csv.DictReader(fin)
                if 'sqlite' in current_app.config['SQLALCHEMY_DATABASE_URI']:
                    for i in dr:
                        user = User(
                            id=i['id'], active=i['_active'], login=i['login'], password=generate_password_hash(i['password']), partner_id=i['partner_id'], notification_type=i['notification_type'])                    
                        db.session.add(user)
                        db.session.commit()
