from app import db

from sqlalchemy import Float
from sqlalchemy.ext.declarative import declared_attr

import re
import enum

CURRENCY_DISPLAY_PATTERN = re.compile(r'(\w+)\s*(?:\((.*)\))?')


class PositionEnum(enum.Enum):
    after = "After Amount"
    before = "Before Amount"    


class Currency(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(10), nullable=False) # Currency Code (ISO 4217)
    rate = db.Column(Float)
    rate_ids = db.relationship('CurrencyRate', backref='rates_for_this_currency')
    rounding = db.Column(Float, default=0.01) # rounding factor
    decimal_places = db.Column(db.Integer)
    active = db.Column(db.Boolean, default=True)
    position = db.Column(
        db.Enum(PositionEnum),
        default=PositionEnum.after
    )
    date = db.Column(db.Date)
    currency_unit_label = db.Column(db.String(10)) # currency unit name
    currency_subunit_label = db.Column(db.String(10)) # currency subunit name
    partnerbank_ids = db.relationship('PartnerBank', backref='partner_bank_currencies')
    country_ids = db.relationship('Country', backref='countries_using_this_currency')  
    

    def _compute_current_rate(self):
        pass
    
    def _get_rates(self, company, date):
        pass


class CurrencyRate(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Date, nullable=False, index=True)
    rate = db.Column(Float, default=1.0) # The rate of the currency to the currency of rate 1
    currency_id = db.Column(db.Integer, db.ForeignKey('currency.id'))
    company_id = db.Column(db.Integer, db.ForeignKey('company.id'))
    
