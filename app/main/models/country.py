from app import db

import enum

FLAG_MAPPING = {
    "GF": "fr",
    "BV": "no",
    "BQ": "nl",
    "GP": "fr",
    "HM": "au",
    "YT": "fr",
    "RE": "fr",
    "MF": "fr",
    "UM": "us",
}

NO_FLAG_COUNTRIES = [
    "AQ", #Antarctica
    "SJ", #Svalbard + Jan Mayen : separate jurisdictions : no dedicated flag
]

class NamePositionEnum(enum.Enum):
    before = "Before Address"
    after = "After Address"


class Country(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), nullable=False) # Full country name
    code = db.Column(db.String(5)) # ISO country code

    """
        Display format to use for addresses belonging to this country.\n\n"
             "You can use python-style string pattern with all the fields of the address "
             "(for example, use '%(street)s' to display the field 'street') plus"
             "\n%(state_name)s: the name of the state"
             "\n%(state_code)s: the code of the state"
             "\n%(country_name)s: the name of the country"
             "\n%(country_code)s: the code of the country
    """
    address_format = db.Column(db.Text, nullable=False, default='%(street)s\n%(street2)s\n%(city)s %(state_code)s %(zip)s\n%(country_name)s') 

    # address_view_id =
    
    banks = db.relationship("Bank", backref="banks_found_in_this_country")    
    companies = db.relationship("Company", backref="companies_found_in_this_country")
    currency_id = db.Column(db.Integer, db.ForeignKey('currency.id'))
    image_url = db.Column(db.String(120)) # url of flag image
    phone_code = db.Column(db.Integer) # country calling code
    name_position = db.Column(
        db.Enum(NamePositionEnum),
        default=NamePositionEnum.before
    ) # determine where the customer/company name should be placed i.e before or after address
    vat_label = db.Column(db.String(60)) # use this field if you want to change vat label
    state_required = db.Column(db.Boolean, default=False)
    zip_required = db.Column(db.Boolean, default=True)
    partners = db.relationship("Partner", backref="partners_assosciated_with_this_country")

    
    