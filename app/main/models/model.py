from app import db

class ModelAccess(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), nullable=False, index=True)

    # If you uncheck the active field, it will diable the 
    # ACL without deleting it (if you delete a native ACL, 
    # it will be re-created when you reload the module).
    active = db.Column(db.Boolean, default=True)
    model_id = db.Column(db.Integer, db.ForeignKey('model.id'), index=True)
    group_id = db.Column(db.Integer, db.ForeignKey('user_group.id', ondelete='RESTRICT'), index=True)
    perm_read = db.Column(db.Boolean) # Read Access
    perm_write = db.Column(db.Boolean) # Write Access
    perm_create = db.Column(db.Boolean) # Create Access
    perm_unlink = db.Column(db.Boolean) # Delete Access


class Model(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    access_ids = db.relationship('ModelAccess', backref='access_ids_for_this_model')
    rules = db.relationship('Rule', backref='rules_for_this_model')
    
    