from app import db, current_app
from app.main.models.user import rule_groups_helper
from config import basedir

import csv
import os


class Rule(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), index=True)

    # If you uncheck the active field, it will disable
    # the record rule without deleting it (if you delete
    # a native record rule, it may be re-created when you
    # reload the module)
    active = db.Column(db.Boolean, default=True)    
    model_id = db.Column(db.Integer, db.ForeignKey('model.id'))    
    groups = db.relationship('UserGroup', secondary=rule_groups_helper, lazy='subquery',
                             backref=db.backref('rules', lazy=True))
    domain_force = db.Column(db.Text) # domain
    perm_read = db.Column(db.Boolean, default=True) # Apply for Read
    perm_write = db.Column(db.Boolean, default=True) # Apply for Write
    perm_create = db.Column(db.Boolean, default=True) # Apply for Create
    perm_unlink = db.Column(db.Boolean, default=True) # Apply for Delete    

    @staticmethod
    def seed_rules():
        rule = Rule.query.first()
        if rule is None:
            csv_file = os.path.join(
                basedir, 'app/main/data/rules.csv')
            with open(csv_file, 'r') as fin:
                dr = csv.DictReader(fin)
                if 'sqlite' in current_app.config['SQLALCHEMY_DATABASE_URI']:
                    for i in dr:
                        rule = Rule(
                            id = i['id'], name=i['name'], active=i['active'], model_id=i['model_id'], domain_force=i['domain_force'], perm_read='t' if i['perm_read'] == 't' else 'f', perm_write='t' if i['perm_write'] == 't' else 'f', perm_create='t' if i['perm_create'] == 't' else 'f', perm_unlink='t' if i['perm_unlink'] == 't' else 'f')
                        db.session.add(rule)
                        db.session.commit()

    
