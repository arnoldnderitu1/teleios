from app import db

import enum

company_users = db.Table('company_users', 
    db.Column('user_id', db.ForeignKey('user.id')),
    db.Column('company_id', db.ForeignKey('company.id'))
)

class BaseOnboardingEnum(enum.Enum):
    not_done = "Not done"
    just_done = "Just done"
    done = "Done"


class FontSelectionEnum(enum.Enum):
    Lato = "Lato"
    Roboto = "Roboto"
    Open_Sans = "Open Sans"
    Montserrat = "Montserrat"
    Oswald = "Oswald"
    Raleway = "Raleway"



class Company(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False) # Company Name
    sequence = db.Column(db.Integer, default=10) # used to order companies in the company switcher
    parent_id = db.Column(db.Integer, db.ForeignKey('company.id')) # Parent Company
    child_companies = db.relationship("Company", backref=db.backref('Company', remote_side=[id]))
    report_header = db.Column(db.Text) # company tagline
    report_footer = db.Column(db.Text)
    user_ids = db.relationship('User', secondary=company_users, lazy='subquery', backref=db.backref('accepted_users', lazy=True)) 
    street = db.Column(db.String(30))
    street2 = db.Column(db.String(30))
    zip = db.Column(db.String(30))
    city = db.Column(db.String(30))
    partnerbank_ids = db.relationship('PartnerBank', backref='bankaccounts_assosciated_with_this_company')    
    country_id = db.Column(db.Integer, db.ForeignKey('country.id'))
    email = db.Column(db.String(10))
    phone = db.Column(db.String(20))
    website = db.Column(db.String(60))
    vat = db.Column(db.String(15)) # Tax ID
    company_registry = db.Column(db.String(60))
    
    # paperformat_id =
    # external_report_layout_id =
    
    base_onboarding_company_state = db.Column(
        db.Enum(BaseOnboardingEnum),
        default=BaseOnboardingEnum.not_done
    ) # state of onboarding company step
    
    # favicon = 

    font = db.Column(
        db.Enum(FontSelectionEnum),
        default=FontSelectionEnum.Lato
    )
    primary_color = db.Column(db.String(30))
    secondary_color = db.Column(db.String(30))
    currencyrate_ids = db.relationship('CurrencyRate', backref='rates_assosciated_with_this_company')    
    partner_ids = db.relationship('Partner', backref='partner_accounts_linked_to_this_company')


    
