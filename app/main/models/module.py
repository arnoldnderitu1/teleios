from app import db, current_app
from config import basedir
from app.models import AuditTrailMixin
from io import TextIOWrapper
import csv
import os
import enum

class StateEnum(enum.Enum):
    uninstallable = 'Uninstallable'
    uninstalled = 'Not Installed'
    installed = 'Installed'
    to_upgrade = 'To be upgraded'
    to_remove = 'To be removed'
    to_install = 'To be installed'


class LicenseEnum(enum.Enum):
    GPL_2 = 'GPL Version 2'
    GPL_2_or_any_later_version = 'GPL-2 or later version'
    GPL_3 = 'GPL Version 3'
    GPL_3_or_any_later_version = 'GPL-3 or later version'
    AGPL_3 = 'Affero GPL-3'
    LGPL_3 = 'LGPL Version 3'
    Other_OSI_approved_licence = 'Other OSI Approved License'
    OEEL_1 = 'Teleios Enterprise Edition License v1.0'
    OPL_1 = 'Teleios Proprietary License v1.0'
    Other_proprietary = 'Other Proprietary'


class ModuleCategory(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128))
    description = db.Column(db.Text)
    # nioicon = db.Column(db.String(200))
    visible = db.Column(db.Boolean, default=True)
    sequence = db.Column(db.Integer, index=True)
    modules = db.relationship('Module', backref='modules_in_this_category', lazy='dynamic')
    user_groups = db.relationship(
        'UserGroup', backref='applications', lazy=True)
    parent_id = db.Column(db.Integer, db.ForeignKey('module_category.id'))
    children = db.relationship('ModuleCategory',
                               backref=db.backref('parent', remote_side=[id])
                               )

    @staticmethod
    def insert_categories():
        category = ModuleCategory.query.first()
        if category is None:
            csv_file = os.path.join(
                basedir, 'app/main/data/module_category.csv')
            with open(csv_file, 'r') as fin:
                dr = csv.DictReader(fin)
                if 'sqlite' in current_app.config['SQLALCHEMY_DATABASE_URI']:
                    for i in dr:
                        category = ModuleCategory(
                            id=i['id'], name=i['name'], description=i['description'], visible=i['_visible'], sequence=i['sequence'], parent_id=i['parent_id'])
                        db.session.add(category)
                        db.session.commit()


class Module(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), index=True) # technical name
    category_id = db.Column(db.Integer, db.ForeignKey('module_category.id'))
    shortdesc = db.Column(db.String(60)) # module name
    summary = db.Column(db.String(120))
    description = db.Column(db.Text)
    description_html = db.Column(db.Text)
    author = db.Column(db.String(60)) 
    maintainer = db.Column(db.String(60))
    contributors = db.Column(db.Text)
    website = db.Column(db.String(120))

    # attention: Incorrect field names !!
    #   installed_version refers the latest version (the one on disk)
    #   latest_version refers the installed version (the one in database)
    #   published_version refers the version available on the repository

    installed_version = db.Column(db.String(60))
    latest_version = db.Column(db.String(60))
    published_version = db.Column(db.String(60))

    url = db.Column(db.String(60))
    sequence = db.Column(db.Integer, default=100)
    # dependencies_id = fields.One2many('ir.module.module.dependency', 'module_id',
    #                                    string='Dependencies', readonly=True)

    # exclusion_ids = fields.One2many('ir.module.module.exclusion', 'module_id',
    #                                 string='Exclusions', readonly=True)

    auto_install = db.Column(db.Boolean) # An auto-installable module is automatically installed by the '
                                         # 'system when all its dependencies are satisfied. '
                                         # 'If the module has no dependency, it is always installed.
    
    state = db.Column(
        db.Enum(StateEnum),
        default=StateEnum.uninstallable,
        index=True
    )
    demo = db.Column(db.Boolean, default=False) # Demo Data 
    license = db.Column(
        db.Enum(LicenseEnum),
        default=LicenseEnum.LGPL_3
    )
    menus_by_module = db.Column(db.Text) # menus
    reports_by_module = db.Column(db.Text) # reports
    views_by_module = db.Column(db.Text) # views
    application = db.Column(db.Boolean) # application
    icon = db.Column(db.String(60)) # icon url
    icon_image = db.Column(db.String(120))
    to_buy = db.Column(db.Boolean, default=False) # Teleios Enterprise Module
    has_iap = db.Column(db.Boolean, default=False)


    @staticmethod
    def insert_modules():
        module = Module.query.first()
        if module is None:
            pass