from app import db, current_app
from config import basedir
from operator import itemgetter

import enum
import os
import csv

DEFAULT_DATE_FORMAT = '%m/%d/%Y'
DEFAULT_TIME_FORMAT = '%H:%M:%S'


class DirectionEnum(enum.Enum):
    ltr = "Left-to-right"
    rtl = "Rigt-to-Left"


class WeekStartEnum(enum.Enum):
    one = "Monday"
    two = "Tuesday"
    three = "Wednesday"
    four = "Thursday"
    five = "Friday"
    six = "Saturday"
    seven = "Sunday"


class Lang(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), nullable=False)
    # used to get/set locales for user
    code = db.Column(db.String(10), nullable=False)
    # name of the po files to use for translations
    iso_code = db.Column(db.String(10))
    # Lang code displayed in the URL
    url_code = db.Column(db.String(120), nullable=False)
    active = db.Column(db.Boolean)
    directions = db.Column(
        db.Enum(DirectionEnum),
        default=DirectionEnum.ltr,
        nullable=False
    )
    date_format = db.Column(db.String(60), nullable=False,
                            default=DEFAULT_DATE_FORMAT)
    time_format = db.Column(db.String(60), nullable=False,
                            default=DEFAULT_TIME_FORMAT)
    week_start = db.Column(
        db.Enum(WeekStartEnum),
        default=WeekStartEnum.seven,
        nullable=False
    )  # First day of the week

    """ 
    Separator Format. It should be like [,n] where 0 < n :starting from Unit digit.
    -1 will end the separation. e.g [3,2,-1] will represent 106500, to be 1,06,500;
    [1,2,-1] will represent it to be [106,05,0]; [3] will represent it as 106,500.
    Provided ',' as the thousand separator in each case
    """
    grouping = db.Column(db.String(10), nullable=False, default='[]')
    decimal_point = db.Column(
        db.String(5), nullable=False, default='.')  # decimal separator
    thousand_sep = db.Column(db.String(5), nullable=False,
                             default=',')  # thousands separator

    @staticmethod
    def seed_language():
        lang = Lang.query.first()
        if lang is None:
            csv_file = os.path.join(
                basedir, 'app/main/data/language.csv')
            with open(csv_file, 'r') as fin:
                dr = csv.DictReader(fin)
                if 'sqlite' in current_app.config['SQLALCHEMY_DATABASE_URI']:
                    for i in dr:
                        lang = Lang(
                            id=i['id'], name=i['name'], code=i['code'], iso_code=i['iso_code'], url_code=i['url_code'], active=i['_active'], directions=i['direction'], date_format=i['date_format'], time_format=i['time_format'], grouping=i['grouping'], decimal_point=i['decimal_point'], thousand_sep=i['thousands_sep'])
                        db.session.add(lang)
                        db.session.commit()

    def get_installed(self):
        """Return the installed languages as a list of (code, name) sorted by name"""
        langs = self.with_context(activate_test=True).search([])
        return sorted([(lang.code, lang.name) for lang in langs], key=itemgetter(1))
