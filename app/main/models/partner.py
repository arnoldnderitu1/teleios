import pytz
import enum
import datetime
import csv
import os

from sqlalchemy import Float

from app import db, current_app
from config import basedir


_tzs = [(tz, tz) for tz in sorted(pytz.all_timezones,
                                  key=lambda tz: tz if not tz.startswith('Etc/') else '_')]

TimeZoneEnum = enum.Enum('TimeZoneEnum', _tzs)



partner_categories = db.Table('partner_categories',
                              db.Column('partner_id', db.Integer,
                                        db.ForeignKey('partner.id')),
                              db.Column('category_id', db.Integer,
                                        db.ForeignKey('partner_category.id'))
                              )


class AddressTypeEnum(enum.Enum):
    contact = 'Contact'
    invoice = 'Invoice Address'
    delivery = 'Delivery Address'
    other = 'Other Address'
    private = "Private Address"


class Partner(db.Model):
    __tablename__ = "partner"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), index=True)
    date = db.Column(db.Date, index=True)
    type = db.Column(db.String(50))
    display_name = db.Column(db.String(60), index=True)
    title_id = db.Column(db.Integer, db.ForeignKey('partner_title.id'))
    parent_id = db.Column(db.Integer, db.ForeignKey('partner.id'))
    children = db.relationship(
        'Partner', backref=db.backref('parent', remote_side=[id]), foreign_keys=[parent_id])
    ref = db.Column(db.String(60), index=True)
    lang = db.Column(db.String(30)) # All emails and documents sent to this contact will be translated in this language
    tz = db.Column(db.String(30))
    active_lang_count = db.Column(db.Integer())

    # the internal user in charge of this contact
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    vat = db.Column(db.String(30), index=True)  # Tax ID.
    same_vat_partner_id = db.Column(db.Integer(), db.ForeignKey('partner.id'))
    partners_with_same_vat_id = db.relationship(
        'Partner', backref=db.backref('same_vat_id'), remote_side=[id], foreign_keys=[same_vat_partner_id])
    bank_ids = db.relationship(
        "PartnerBank", backref="banks_linked_to_this_partner")
    website = db.Column(db.String(120))  # website link
    comment = db.Column(db.Text)  # notes
    category_id = db.relationship('PartnerCategory', secondary=partner_categories,
                                  backref='categories_assosciated_with_this_partner')

    credit_limit = db.Column(db.String)  # Credit Limit
    debit_limit = db.Column(db.String)  # Credit Limit
    active = db.Column(db.Boolean, default=True)
    # Check this box if this contact is an Employee.
    employee = db.Column(db.Boolean)
    function = db.Column(db.String(30))  # Job Position
    Addresstype = db.Column(db.String(30), default="Contact") # Address Type. Invoice & Delivery addresses are used in sales orders. Private addresses are only visible by authorized users
    
    street = db.Column(db.String(60))
    street2 = db.Column(db.String(60))
    zip = db.Column(db.String(60))
    city = db.Column(db.String(60))
    
    # state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict', domain="[('country_id', '=?', country_id)]")
    
    country_id = db.Column(db.Integer, db.ForeignKey('country.id')) 
    partner_latitude = db.Column(Float) #Geo Latitude
    partner_longitude = db.Column(Float) #Geo Longitude
    email = db.Column(db.String(60))
    email_formatted = db.Column(db.String(60)) # Formatted Email Address => Name <email@domain>
    phone = db.Column(db.String(20))
    mobile = db.Column(db.String(20))
    is_company = db.Column(db.Boolean, default=False) #Is a Company. Check if the contact is a company, otherwise it is a person
    
    # industry_id = fields.Many2one('res.partner.industry', 'Industry')
    
    # company_type is only an interface field, do not use it in business logic

    company_id = db.Column(db.Integer, db.ForeignKey('company.id'))
    color = db.Column(db.Integer, default=0) #Color Index
    
    user_ids = db.relationship('User', backref='users_linked_to_this_partner_account', foreign_keys=[user_id])
    partner_share = db.Column(db.Boolean) # Share Partner
                                          # Either customer (not a user), either shared user. Indicated the current partner is a customer without access or with a limited access created for sharing data
    contact_address = db.Column(db.String(60)) #Complete Address

    # technical field used for managing commercial fields
    commercial_partner_id = db.Column(db.Integer, db.ForeignKey('partner.id')) #Commercial Entity
    child_commercial_partner_id = db.relationship('Partner', backref=db.backref('child_commercial_partner_entities', remote_side=[id]), foreign_keys=[commercial_partner_id])

    commercial_company_name = db.Column(db.String(30)) #Company Name Entity
    company_name = db.Column(db.String(30)) #Company Name
    barcode = db.Column(db.String(30)) #Use a barcode to identify this contact
    supplier_rank = db.Column(db.Integer)
    customer_rank = db.Column(db.Integer)

    __mapper_args__ = {
        'polymorphic_identity': 'partner',
        'polymorphic_on': Addresstype
    }

    @staticmethod
    def insert_partners():
        partner = Partner.query.first()
        if partner is None:
            csv_file = os.path.join(
                basedir, 'app/main/data/partners.csv')
            with open(csv_file, 'r') as fin:
                dr = csv.DictReader(fin)
                if 'sqlite' in current_app.config['SQLALCHEMY_DATABASE_URI']:
                    for i in dr:
                        partner = Partner(
                            id=i['id'], name=i['name'], company_id=i['company_id'], display_name=i['display_name'], parent_id=i['parent_id'], lang=i['lang'], tz=i['tz'], website=i['website'], active=i['_active'], function=i['function'], type=i['type'], street=i['street'], city=i['city'], country_id=i['country_id'], email=i['email'], phone=i['phone'], is_company=i['_is_company'], color=i['color'], commercial_partner_id=i['commercial_partner_id'], commercial_company_name=i['commercial_company_name'], company_name=i['company_name'], debit_limit=i['debit_limit'], supplier_rank=i['supplier_rank'], customer_rank=i['customer_rank'])                    
                        db.session.add(partner)
                        db.session.commit()
                    


    def _compute_active_lang_count(self):
        lang_count = len(self.env['lang'].get_installed())
        for partner in self:
            partner.active_lang_count = lang_count

    def _compute_tz_offset(self):
        for partner in self:
            partner.tz_offset = datetime.datetime.now(
                pytz.timezone(partner.tz or 'GMT')).strftime('%z')

    def _compute_same_vat_partner_id(self):
        for partner in self:
            # use _origin to deal with onchange()
            partner_id = partner._origin.id
            # active_test = False becuase if a partner has been deactivated you still want to raise the error
            # so that you can reactivate it instead of creating a new one, which would loose its history
            Partner = self.with_context(active_test=False).sudo()
            domain = [
                ('vat', '=', partner.vat)
                ('company_id', 'in', [False, partner.company_id.id]),
            ]
            if partner_id:
                domain += [('id', '!=', partner_id), '!',
                           ('id', 'child_of', partner_id)]
            partner.same_vat_partner_id = bool(
                partner.vat) and not partner.parent_id and Partner.search(domain, limit=1)


class PartnerTitle(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), nullable=False)
    shortcut = db.Column(db.String(10))  # Abbreviation
    partners = db.relationship(
        'Partner', backref='partners_assosciated_with_this_partner')


class PartnerCategory(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(10), nullable=False)  # Tag Name
    color = db.Column(db.Integer)  # Color Index
    parent_id = db.Column(db.Integer, db.ForeignKey('partner_category.id'))
    child_ids = db.relationship('PartnerCategory', backref=db.backref(
        'child_categories', remote_side=[id]))
    # The active field allows you to hide the category without removing it.
    active = db.Column(db.Boolean, default=True)
    parent_path = db.Column(db.String(60), index=True)
    partner_ids = db.relationship(
        'Partner', secondary=partner_categories, backref='partners_assosciated_with_this_category')
