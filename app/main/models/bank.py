from app import db

class Bank(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), nullable=False)
    street = db.Column(db.String(60))
    street2 = db.Column(db.String(60))
    zip = db.Column(db.String(60))
    city = db.Column(db.String(60))
    state = db.Column(db.String(60))
    partnerbank_ids = db.relationship("PartnerBank", backref="partnerbanks")
    country_id = db.Column(db.Integer, db.ForeignKey('country.id'))
    


class PartnerBank(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    active = db.Column(db.Boolean, default=True)
    acc_number = db.Column(db.String(30), nullable=False)
    acc_holder_name = db.Column(db.String(30), nullable=False) # Account holder name
    partner_id = db.Column(db.Integer, db.ForeignKey('partner.id'))
    bank_id = db.Column(db.Integer, db.ForeignKey('bank.id'))
    currency_id = db.Column(db.Integer, db.ForeignKey('currency.id'))
    company_id = db.Column(db.Integer, db.ForeignKey('company.id'))

    