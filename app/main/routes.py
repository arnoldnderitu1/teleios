from datetime import datetime
from flask import render_template, flash, redirect, url_for, request, g, \
    jsonify, current_app
from flask_login import current_user, login_required
from flask_babel import _, get_locale
# from guess_language import guess_language
from app import db
from app.main.forms import EditProfileForm, EmptyForm, PostForm, SearchForm, \
    MessageForm
from app.auth.models.employee import Employee
from app.main.models.module import Module, ModuleCategory
from app.translate import translate
from app.main import bp
from app.decorators import admin_required


@bp.route('/', methods=['GET', 'POST'])
@login_required
def index():
    return render_template('index.html', title=_('Tool to Grow Your Business | Teleios'))


@bp.route('/new/database')
@login_required
@admin_required
def choose_apps():
    moduleCategories = ModuleCategory.query.join(
        Module, ModuleCategory.id == Module.category_id).filter(Module.enable.is_(True)).all()
    modules = Module.query.filter(Module.enable.is_(True)).all()
    return render_template('main/set-up.html', title=_('New Database | Teleios'), moduleCategories=moduleCategories, modules=modules)


@bp.route('/getting-started/databases')
@login_required
@admin_required
def getting_started():
    return render_template('main/set-up.html', title=_('Getting Started | Teleios'))


@bp.route('/home')
@login_required
def home():
    modules = Module.query.join(ModuleCategory, Module.category_id == ModuleCategory.id).filter(
        Module.installed.is_(True)).all()
    return render_template('main/home.html', title=_('Home | Teleios'), modules=modules)


@bp.route('/module/<id>')
@login_required
def connect(id):
    module = Module.query.filter_by(id=id).first()
    descriptor = module.descriptor
    if descriptor == "accounting":
        return redirect(url_for('accounting.dashboard'))


@bp.route('/dashboard')
@login_required
def dashboard():
    return render_template('main/dashboard.html', title=_('Dashboard | Teleios'))


@bp.route('/all-apps', methods=['GET', 'POST'])
@login_required
def all_apps():
    modulecategories = ModuleCategory.query.all()
    _modules = Module.query.all()
    return render_template('main/apps.html', title=_('Teleios | All Apps'), modulecategories=modulecategories, _modules=_modules, )


@bp.route('/module/<name>', methods=['GET', 'POST'])
@login_required
def _module(name):
    return render_template('base.html', title=_('Teleios | ' + name))


@bp.route('/install_module/<id>', methods=['GET', 'POST'])
@admin_required
def install_module(id):
    """Installs the selected module"""
    # get module details
    module = Module.query.filter_by(id=id).first()
    return "success"
