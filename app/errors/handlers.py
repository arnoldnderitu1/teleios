from flask import render_template, request
from app import db
from app.errors import bp
from app.api.errors import error_response as api_error_response


def wants_json_response():
    return request.accept_mimetypes['application/json'] >= \
        request.accept_mimetypes['text/html']


@bp.app_errorhandler(404)
def not_found_error(error):
    if wants_json_response():
        return api_error_response(404)
    return render_template('errors/404.html'), 404


@bp.app_errorhandler(403)
def forbidden_error(error):
    if wants_json_response():
        return api_error_response(403)
    return render_template('errors/403.html'), 403


@bp.app_errorhandler(500)
def internal_error(error):
    db.session.rollback()
    if wants_json_response():
        return api_error_response(500)
    return render_template('errors/500.html'), 500


@bp.app_errorhandler(500)
def validation_error(error):
    """
    Violation of python constraints/

    ..admonition:: Example

        When you try to create a new user with a login which already exists in the db.
    """
    db.session.rollback()
    if wants_json_response():
        return api_error_response(501)
    return render_template('errors/501.html')


@bp.app_errorhandler(400)
def user_error(error):
    """Generic error managed by the client.

    Typically when the user tries to do something that has no sense given the current
    state of a record. Semantically comparable to the generic 400 HTTP status codes.
    """
    db.session.rollback()
    if wants_json_response():
        return api_error_response(501)
    return render_template('errors/400.html')
