from flask import redirect, render_template
from app.accounting import bp
from flask_babel import _, get_locale
from app.main.models.module import Module


@bp.route('/overview')
def dashboard():
    module = Module.query.filter_by(descriptor='accounting').first()
    return render_template('accounting/dashboard.html', title=_('Accounting Dashboard | Teleios'), module=module)


@bp.route('/customers')
def customers():
    module = Module.query.filter_by(descriptor='accounting').first()
    return render_template('accounting/dashboard.html', title=_('Accounting Dashboard | Teleios'), module=module)


@bp.route('/vendors')
def vendors():
    module = Module.query.filter_by(descriptor='accounting').first()
    return render_template('accounting/dashboard.html', title=_('Accounting Dashboard | Teleios'), module=module)


@bp.route('/')
def accounting():
    module = Module.query.filter_by(descriptor='accounting').first()
    return render_template('accounting/dashboard.html', title=_('Accounting Dashboard | Teleios'), module=module)


@bp.route('/reporting')
def reporting():
    module = Module.query.filter_by(descriptor='accounting').first()
    return render_template('accounting/dashboard.html', title=_('Accounting Dashboard | Teleios'), module=module)


@bp.route('/configuration')
def configuration():
    module = Module.query.filter_by(descriptor='accounting').first()
    return render_template('accounting/dashboard.html', title=_('Accounting Dashboard | Teleios'), module=module)


@bp.route('/chart-of-accounts')
def coa():
    accounts = ""
    return render_template('accounting/coa.html', title=_('Chart of Accounts | Teleios'), accounts=accounts)
