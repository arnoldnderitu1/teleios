from flask import render_template, flash, redirect, url_for, request, g, \
    jsonify, current_app
from flask_login import current_user, login_required
from flask_babel import _, get_locale
from guess_language import guess_language
from app import db
from app.sales import bp
# from app.sales.models.sales import SalesDocument
from app.decorators import admin_required


def get_salesdocuments():
    # salesdocuments = SalesDocument.query.all()
    return "salesdocuments"


@bp.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    modules = {
        "name": "Sales",
        "icon": '<em class="icon ni ni-report-profit"></em>'
    }
    salesdocuments = get_salesdocuments()
    return render_template('sales/index.html', title=_('Sales Module | Teleios'), modules=modules, salesdocuments=salesdocuments)


@bp.route('/dashboard', methods=['GET', 'POST'])
@login_required
def dashboard():
    modules = {
        "name": "Sales",
        "icon": '<em class="icon ni ni-report-profit"></em>'
    }
    salesdocuments = get_salesdocuments()
    return render_template('sales/index.html', title=_('Sales Dashboard | Teleios'), modules=modules, salesdocuments=salesdocuments)


@bp.route('/<document>/', methods=['GET', 'POST'])
def document(document):
    modules = {
        "name": "Sales",
        "icon": '<em class="icon ni ni-report-profit"></em>'
    }
    salesdocuments = SalesDocument.query.all()
    return render_template('sales/index.html', title=_('Sales - ' + document + ' | Teleios'), modules=modules, salesdocuments=salesdocuments)


@bp.route('/new-quotation')
def new_quotation():
    modules = {
        "name": "Sales",
        "icon": '<em class="icon ni ni-report-profit"></em>'
    }
    salesdocuments = get_salesdocuments()
    return render_template('sales/index.html', title=_('New Quotation | Teleios'), modules=modules, salesdocuments=salesdocuments)
